    import javax.swing.*;
import java.io.*;
import java.net.URL;

/**
 * handle the downloading process in single thread
 */
public class DownloadThread extends Thread implements Serializable {

    private Download download;
    private GUI gui;
    private long start = System.nanoTime();
    private int downloaded = 0;
    private boolean update = true;

    public DownloadThread(Download download, GUI gui) {
        this.download = download;
        this.gui = gui;
    }

    @Override
    public void run() {
        BufferedInputStream in = null;
        FileOutputStream fout = null;
//        DownloadManager.setRunningThreads(DownloadManager.getRunningThreads()+1);

        try {
            URL url = new URL(download.getLink());
            in = new BufferedInputStream(url.openStream());
            fout = new FileOutputStream(download.getSaveLocation() + "/" + download.getFileName());

            while (download.getProgress()<download.getSize()) {
                int count;
//                int net = in.read();
                int net = 10240;
//                System.out.println(net);
                final byte data[] = new byte[net];
                if (update) {
                    start = System.nanoTime();
                    update = false;
                    downloaded = 0;
                }
                count = in.read(data, 0, net);
                downloaded += count;
//                System.out.println(count);
                if (count == -1) {
                    break;
                }
                fout.write(data, 0, count);
                download.setProgress(download.getProgress() + (count) / 1024);
//                System.out.println(download.getSpeed());
                sleep(40);
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        gui.refresh();
                        long end = System.nanoTime();
                        if (end - start > 1000000000){
                            download.setSpeed( (float) downloaded / (end - start) * (1000000000)/1024);
                            update = true;
                        }
                    }
                });
            }
            download.setStatus("Completed");
            DownloadManager.stopDownloading(download);

        } catch (Exception e) {
            System.out.println(e);
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
                if (fout != null) {
                    fout.close();
                }
            } catch (Exception e) {
                System.out.println(e);
            }
        }
//        DownloadManager.setRunningThreads(DownloadManager.getRunningThreads()-1);
    }


}
