import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.*;


/**
 * starts and stops downlaods and manages the queues
 */
public class DownloadManager {

    private static int runningThreads = 0;
    private static  boolean isQueueStarted = false;
    private static int nextQueueDownlaod=0;


    private static ArrayList<Download> downloads;
    private static ArrayList<Download> queue;
    static ExecutorService service;

    static ExecutorService queueService;

    public DownloadManager(ArrayList<Download> downloads, ArrayList<Download> queue) { }

    /**
     * starts the input download
     * @param download
     */
    public static void startDownloading(Download download){
        if (GUI.getLimitForSimultaneouslyDownload()!=-1){
            if (DownloadManager.runningThreads < GUI.getLimitForSimultaneouslyDownload()) {
                synchronized (download.getThread()) {
                    if (download.isStarted()) {
                        download.getThread().interrupt();
                        System.out.println(download.getFileName() + " resumed");
                    } else {
                        download.getThread().start();
                        download.setStarted(true);
                        System.out.println(download.getFileName() + " started");
                    }
                }
                download.setDownloading(true);
                DownloadManager.runningThreads++;
            } else {
                System.out.println(download.getFileName() + " waiting");
                download.setWaiting(true);
            }
    } else {
            synchronized (download.getThread()) {
                if (download.isStarted()) {
                    download.getThread().interrupt();
                    System.out.println(download.getFileName() + " resumed");
                } else {
                    download.getThread().start();
                    System.out.println(download.getFileName() + " started");
                    download.setStarted(true);
                }
            }
            download.setDownloading(true);

            DownloadManager.runningThreads++;
        }
    }

    /**
     * stops the input download
     * @param download
     */
    public static void stopDownloading(Download download)   {
        try {
            synchronized (download.getThread()) {
                download.getThread().suspend();
                System.out.println(download.getFileName() + " stoped");
            }
            download.setDownloading(false);
            if (download.getStatus().equals("Completed")) {
                Calendar cal = Calendar.getInstance();
                cal.setTime(new Date());
                DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
                download.setFinishTime(df.format(cal.getTime()));
                if (queue.contains(download))
                    queue.remove(download);
            }
            runningThreads--;
            for (Download download1 : DownloadManager.getDownloads()){
                if (download1.isWaiting()){
                    startDownloading(download1);
                    download1.setWaiting(false);
                    break;
                }
            }
        } catch (Exception e){
            System.out.println(e);
        }
    }

    /**
     * starts the input queue
     * @param queue
     */
    public static void startQueue(ArrayList<Download> queue){
        isQueueStarted = true;
        int startingDownload = -1;
        for (int i=0; i< queue.size();i++) {
            System.out.println(queue.get(i).getFileName());
            if (!queue.get(i).getStatus().equals("Completed")) {
                startingDownload = i;
                break;
            }
        }
        queueService = Executors.newSingleThreadExecutor();
        if (startingDownload != -1)
            for (int i=startingDownload ;i<queue.size() ;i++) {
                queueService.submit(queue.get(i).getThread());
            }
        System.out.println("queue started");
    }

    /**
     * stops the input queue
     * @param queue
     */
    public static void stopQueue(ArrayList<Download> queue){
        isQueueStarted = false;
//        queueService.shutdown();
        queueService.shutdownNow();
        for (Download download : queue){
            stopDownloading(download);
        }
        System.out.println("queue stoped");
    }

    /**
     * is the queue started or not
     * @return
     */
    public static boolean isQueueStarted() {
        return isQueueStarted;
    }

    /**
     * returns the downloads
     * @return
     */
    public static ArrayList<Download> getDownloads() {
        return downloads;
    }

    /**
     * sets the downloads
     * @param downloads
     */
    public static void setDownloads(ArrayList<Download> downloads) {
        DownloadManager.downloads = downloads;
    }

    /**
     * sets the queue
     * @param queue
     */
    public static void setQueue(ArrayList<Download> queue) {
        DownloadManager.queue = queue;
    }
}