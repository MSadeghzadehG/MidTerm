import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;


/**
 * saves all download information
 */
public class Download implements Serializable{
    private String fileName;
    private String status;
    private boolean isDownloading;
    private float size;
    private String saveLocation;
    private String createTime;
    private String finishTime;
    private String link;
    private float progress;
    private float speed;
    private DownloadThread thread;
    private boolean isStarted;
    private boolean isWaiting;

    /**
     * creates new download
     * @param fileName
     * @param link
     * @param gui
     */
    public Download(String fileName, String link,GUI gui) {
        this.fileName = fileName;
        this.link = link;
        this.progress = 0;
        this.speed = 0;
        this.status = "";
        this.isDownloading = false;
        this.saveLocation = "/home/mahdi/Desktop";
        this.isStarted = false;
        this.isWaiting = false;

        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        this.createTime = df.format(cal.getTime());
        this.finishTime = "-";
        this.thread = new DownloadThread(this,gui);
        try {
            URL url = new URL(this.getLink());
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            this.size = Float.valueOf(connection.getHeaderField("Content-length"))/1024;
            connection.disconnect();
        } catch (Exception e){
            System.out.println(e);
        }
    }

    /**
     * set the downloaded amount
     * @param progress
     */
    public void setProgress(float progress) {
        this.progress = progress;
    }

    /**
     * sets the speed of downloading
     * @param speed
     */
    public void setSpeed(float speed) {
        this.speed = speed;
    }

    /**
     * returns the status of download
     * @return
     */
    public String getStatus() {
        return status;
    }

    /**
     * returns the speed of download
     * @return
     */
    public float getSpeed() {
        return speed;
    }

    /**
     * is the download on progress or not
     * @param downloading
     */
    public void setDownloading(boolean downloading) {
        isDownloading = downloading;
    }

    /**
     * sets the status of download
     * @param status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * returns the create time of download
     * @return
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * returns the finish time of download
     * @return
     */
    public String getFinishTime() {
        return finishTime;
    }

    /**
     * returns the downloaded amount of file
     * @return
     */
    public float getProgress() {
        return progress;
    }

    /**
     * sets the size of download
     * @param size
     */
    public void setSize(float size) {
        this.size = size;
    }

    /**
     * sets the saving location
     * @param saveLocation
     */
    public void setSaveLocation(String saveLocation) {
        this.saveLocation = saveLocation;
    }

    /**
     * returns the file name
     * @return
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * returns the size of file
     * @return
     */
    public float getSize() {
        return size;
    }

    /**
     * returns the link of file
     * @return
     */
    public String getLink() {
        return link;
    }

    /**
     * returns the location of file
     * @return
     */
    public String getSaveLocation() {
        return saveLocation;
    }

    /**
     * is download over limit or not
     * @return
     */
    public boolean isWaiting() {
        return isWaiting;
    }

    /**
     * set the download waiting status
     * @param waiting
     */
    public void setWaiting(boolean waiting) {
        isWaiting = waiting;
    }

    public static Comparator<Download> byName = new Comparator<Download>() {
        @Override
        public int compare(Download download, Download t1) {
            return download.getFileName().toLowerCase().compareTo(t1.getFileName().toLowerCase());
        }
    };

    public static Comparator<Download> bySize = new Comparator<Download>() {
        @Override
        public int compare(Download download, Download t1) {
            return (int)(download.getSize() - t1.getSize());
        }
    };

    public static Comparator<Download> byTime = new Comparator<Download>() {
        @Override
        public int compare(Download download, Download t1) {
            return download.getCreateTime().toLowerCase().compareTo(t1.getCreateTime().toLowerCase());
        }
    };

    /**
     * returns the downloading status
     * @return
     */
    public boolean isDownloading() {
        return isDownloading;
    }

    /**
     * sets download is started or not
     * @param started
     */
    public void setStarted(boolean started) {
        isStarted = started;
    }

    /**
     * returns the downloading thread
     * @return
     */
    public DownloadThread getThread() {
        return thread;
    }

    /**
     * is download started or not
     * @return
     */
    public boolean isStarted() {
        return isStarted;
    }

    /**
     * sets the download finish time
     * @param finishTime
     */
    public void setFinishTime(String finishTime) {
        this.finishTime = finishTime;
    }


}
