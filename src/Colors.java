import java.awt.*;

public class Colors {
    //public final static Color Cyan = new Color(0, 188, 212);

    public final static Color leftBarText = new Color(255,255,255);
    public final static Color DownloadBarText = new Color(52, 52, 52);


    public final static Color leftBarBackground = new Color(55, 71, 79);
    public final static Color topBarBackground = new Color(38, 50, 56);
    public final static Color downloadBarBackground = new Color(144, 164, 174);
    public final static Color downloadPanelBackground = new Color(144, 164, 174);

    public final static Color leftBarHover = new Color(69, 90, 100);
    public final static Color topBarHover = new Color(55, 71, 79);
    public final static Color downloadBarHover = new Color(176, 190, 197);

    public final static Color leftBarOnClick = new Color(69, 90, 100);
    public final static Color topBarOnClick = new Color(69, 90, 100);
    public final static Color downloadBarOnClick = new Color(207, 216, 220);

    public final static Color progressBar = new Color(162, 19, 11);

    public final static Color newFramePanelBackground = new Color(38, 50, 56);
    public final static Color infoPanelBackground = new Color(178, 223, 219);

    public final static Color aboutMeBackground = new Color(38, 50, 56);

//    teal
//      100 #B2DFDB

//    deepPurple
//    100 #D1C4E9
//    200 #B39DDB
//    300 #9575CD
//    400 #7E57C2
//    500 #673AB7
//    600 #5E35B1
//    700 #512DA8
//    800 #4527A0
//    900 #1A237E


//    blueGray
//    50 #ECEFF1
//    100 #CFD8DC
//    200 #B0BEC5
//    300 #90A4AE
//    400 #78909C
//    500 #607D8B
//    600 #546E7A
//    700 #455A64
//    800 #37474F
//    900 #263238
//



}
