import java.io.*;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * save, load, and  data on the files
 */
public class FileManager {
    private final static String path = ".";
    public FileManager() {
    }

    /**
     * saves the object to a file with input name
     * @param object
     * @param name
     * @param <T>
     */
    public static <T> void save(ArrayList<T> object,String name){
        try {
            String fileName = path+"/"+name+".mdm";
            FileOutputStream fos = new FileOutputStream(fileName);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(object);
            oos.close();
        } catch (IOException e){
            System.out.println(e);
        }
    }

    /**
     * loades the data of a file with input name
     * @param name
     * @param <T>
     * @return
     */
    public static <T> ArrayList<T> load(String name){
        try {
            String fileName = path+"/"+name+".mdm";
            FileInputStream fin = new FileInputStream(fileName);
            ObjectInputStream ois = new ObjectInputStream(fin);
            ArrayList<T> iHandler = (ArrayList<T>) ois.readObject();
            ois.close();
            return iHandler;
        } catch (Exception e){
            System.out.println(e);
            return null;
        }
    }

    /**
     * exports data
     */
    public static void export() {
        try {
            FileOutputStream fos = new FileOutputStream("exported.zip");
            ZipOutputStream zipOut = new ZipOutputStream(fos);

            if ((new File("downloads.mdm")).exists()) {
                File fileToZip = new File(path+"/"+"downloads"+".mdm");
                zipFile(fileToZip, fileToZip.getName(), zipOut);
            }
            if ((new File("filters.mdm")).exists()) {
                File fileToZip = new File(path+"/"+"filters"+".mdm");
                zipFile(fileToZip, fileToZip.getName(), zipOut);
            }
            if ((new File("queue.mdm")).exists()) {
                File fileToZip = new File(path+"/"+"queue"+".mdm");
                zipFile(fileToZip, fileToZip.getName(), zipOut);
            }
            if ((new File("removed.mdm")).exists()) {
                File fileToZip = new File(path+"/"+"removed"+".mdm");
                zipFile(fileToZip, fileToZip.getName(), zipOut);
            }
            if ((new File("settings.mdm")).exists()) {
                File fileToZip = new File(path+"/"+"settings"+".mdm");
                zipFile(fileToZip, fileToZip.getName(), zipOut);
            }
            zipOut.close();
            fos.close();



        } catch (Exception e) {
            System.out.println(e);
        }

    }

    /**
     * works with zip type files
     * @param fileToZip
     * @param fileName
     * @param zipOut
     * @throws IOException
     */
    private static void zipFile(File fileToZip, String fileName, ZipOutputStream zipOut) throws IOException {
        if (fileToZip.isHidden()) {
            return;
        }
        if (fileToZip.isDirectory()) {
            File[] children = fileToZip.listFiles();
            for (File childFile : children) {
                zipFile(childFile, fileName + "/" + childFile.getName(), zipOut);
            }
            return;
        }
        FileInputStream fis = new FileInputStream(fileToZip);
        ZipEntry zipEntry = new ZipEntry(fileName);
        zipOut.putNextEntry(zipEntry);
        byte[] bytes = new byte[1024];
        int length;
        while ((length = fis.read(bytes)) >= 0) {
            zipOut.write(bytes, 0, length);
        }
        fis.close();
    }

    /**
     * deletes the input file
     * @param file
     */
    public static void delete(File file){
        if(file.delete())
        {
            System.out.println("File deleted successfully");
        }
        else
        {
            System.out.println("Failed to delete the file");
        }
    }
}
