import java.awt.*;

public class Fonts {
    public final static Font SansFont = new Font("Sans Regular", Font.PLAIN, 12);
    public final static Font UbuntuFont = new Font("Ubuntu Regular", Font.PLAIN, 12);
    public final static Font OpenSansFont = new Font("Open Sans Regular", Font.PLAIN, 12);
//    public final static Font OpenSansFont = new Font("Segoe UI", Font.PLAIN, 12);
}
