import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;


/**
 * makes the UI of MDM
 */
public class GUI implements Serializable {

    private static JFrame frame;
    private JPanel downloadPanel;
    private JPanel rightPanel;
    private JPanel downloadInfoPanel;
    private JPanel downloadInnerPanel;

    private Download focusedDownload;
    private ArrayList<Download> selectedDownloads;
    private String leftBarSelected;
    private ArrayList<Boolean> showInfo;
    private ArrayList<Boolean> queueShowInfo;
    private ArrayList<Boolean> restoreShowInfo;
    private ArrayList<String> sortBy;
    private boolean isDescending;

    private ArrayList<Download> downloads;
    private ArrayList<Download> queue;
    private ArrayList<Download> removed;
    private ArrayList<String> filters;
    private ArrayList<Download> sorted;
    private ArrayList<Download> searchResult;

    private String lookAndFeel;
    private static int limitForSimultaneouslyDownload;
    private String defaultPath;

    /**
     * creates a gui
     */
    public GUI() {
        initUI();
    }

    /**
     * makes basical components
     */
    private void initUI() {
        JPanel topBar;
        JPanel main;
        JPanel leftBar;
        JMenu helpMenu;
        JMenu downloadMenu;
        JMenuItem exit;
        JMenuBar menuBar;

        onLoad();

        setLookAndFeel(lookAndFeel);
        //Create and set up the window.
        frame = new JFrame("MDownloadManager");
        topBar = new JPanel();
        main = new JPanel();
        leftBar = new JPanel(new BorderLayout());
        downloadPanel = new JPanel(new BorderLayout());
        rightPanel = new JPanel(new BorderLayout());
        leftBarSelected = "Processing";

        menuBar = new JMenuBar();
        downloadMenu = new JMenu("Download");
        helpMenu = new JMenu("Help");
        exit = new JMenuItem("Exit");

        frame.setContentPane(main);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        frame.setMinimumSize(new Dimension(750, 500));
        frame.setSize(new Dimension(900, 600));
        frame.setLayout(new BorderLayout());

        frame.getContentPane().add(leftBar, BorderLayout.WEST);
        frame.getContentPane().add(rightPanel, BorderLayout.CENTER);

        rightPanel.add(topBar, BorderLayout.NORTH);

        leftBar.setPreferredSize(new Dimension(250, 0));
        leftBar.setBorder(new EmptyBorder(5, 0, 0, 0));
        leftBar.setBackground(Colors.leftBarBackground);


        TrayIcon trayIcon;
        if (SystemTray.isSupported()) {
            SystemTray tray = SystemTray.getSystemTray();
            Image image = Toolkit.getDefaultToolkit().getImage("resources/resources/16/ezgif.com-apng-to-gif.gif");
            // create a action listener to listen for default action executed on the tray icon
            ActionListener listener = new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    if (!frame.isVisible()) {
                        setVisible();
                    }
                }
            };
//
//            // create a popup menu
//            PopupMenu popup = new PopupMenu();
//            // create menu item for the default action
//            MenuItem defaultItem = new MenuItem("item");
//            defaultItem.addActionListener(listener);
//            popup.add(defaultItem);
            trayIcon = new TrayIcon(image, "MDM");

            trayIcon.addActionListener(listener);
            try {
                tray.add(trayIcon);
            } catch (AWTException e) {
                System.err.println(e);
            }
        } else {
        }

        //Top Bar

        topBar.setBackground(Colors.topBarBackground);
        JButton newDownloadBtn = new JButton();
        newDownloadBtn.setToolTipText("New Download");
        JButton pauseBtn = new JButton();
        pauseBtn.setToolTipText("Pause Download");
        JButton resumeBtn = new JButton();
        resumeBtn.setToolTipText("Resume Download");
        JButton cancelBtn = new JButton();
        cancelBtn.setToolTipText("Cancel Download");
        JButton removeBtn = new JButton();
        removeBtn.setToolTipText("Remove Download");
        JButton sortBtn = new JButton();
        sortBtn.setToolTipText("Sort Downloads");
        JButton searchBtn = new JButton();
        searchBtn.setToolTipText("Search in Downloads");
        JButton settingBtn = new JButton();
        settingBtn.setToolTipText("Settings");

        try {
            Image img = ImageIO.read(getClass().getResource("resources/resources/32/icons8_Add_32px.png"));
            newDownloadBtn.setIcon(new ImageIcon(img));
            img = ImageIO.read(getClass().getResource("resources/resources/32/icons8_Circled_Play_32px.png"));
            resumeBtn.setIcon(new ImageIcon(img));
            img = ImageIO.read(getClass().getResource("resources/resources/32/icons8_Pause_Button_32px.png"));
            pauseBtn.setIcon(new ImageIcon(img));
            img = ImageIO.read(getClass().getResource("resources/resources/32/icons8_Cancel_32px.png"));
            cancelBtn.setIcon(new ImageIcon(img));
            img = ImageIO.read(getClass().getResource("resources/resources/32/icons8_Trash_32px.png"));
            removeBtn.setIcon(new ImageIcon(img));
            img = ImageIO.read(getClass().getResource("resources/resources/32/icons8-descending-sorting-filled-32.png"));
            sortBtn.setIcon(new ImageIcon(img));
            img = ImageIO.read(getClass().getResource("resources/resources/32/icons8-search-32.png"));
            searchBtn.setIcon(new ImageIcon(img));
            img = ImageIO.read(getClass().getResource("resources/resources/32/icons8_Settings_32px.png"));
            settingBtn.setIcon(new ImageIcon(img));
        } catch (Exception ex) {
            System.out.println(ex);
        }

        ArrayList<JButton> topBarButtons = new ArrayList<>();
        topBarButtons.add(newDownloadBtn);
        topBarButtons.add(resumeBtn);
        topBarButtons.add(pauseBtn);
        topBarButtons.add(cancelBtn);
        topBarButtons.add(removeBtn);
        topBarButtons.add(sortBtn);
        topBarButtons.add(searchBtn);
        topBarButtons.add(settingBtn);

        for (JButton button : topBarButtons) {
            button.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 10));
            button.setBackground(Colors.topBarBackground);
            button.setFocusPainted(false);
            button.setName(button.getToolTipText());
            //button.setContentAreaFilled(false);
            button.addMouseListener(new ButtonsAdaptor());
            topBar.add(button);
        }


        //Left Bar

        JPanel leftBarPanel = new JPanel(new GridLayout(2, 1));
        JPanel innerLeftBarPanel = new JPanel(new GridLayout(4, 1));
        leftBarPanel.setBackground(Colors.leftBarBackground);
        innerLeftBarPanel.setBackground(new Color(50, 54, 63));
        JButton leftBarLabel1 = new JButton("Processing");
        leftBarLabel1.setName("Processing");
        leftBarLabel1.setToolTipText("Downloads in Process");
        JButton leftBarLabel2 = new JButton("Completed");
        leftBarLabel2.setName("Completed");
        leftBarLabel2.setToolTipText("Completed Downloads");
        JButton leftBarLabel3 = new JButton("Queues");
        leftBarLabel3.setName("Queues");
        leftBarLabel3.setToolTipText("Downloads in Queues");
        JButton restoreBtn = new JButton("Restore");
        restoreBtn.setToolTipText("Restore Removed Downloads");
        restoreBtn.setName("Restore");
        ArrayList<JButton> leftBarLabels = new ArrayList<>();
        leftBarLabels.add(leftBarLabel1);
        leftBarLabels.add(leftBarLabel2);
        leftBarLabels.add(leftBarLabel3);
        leftBarLabels.add(restoreBtn);

        try {
            Image img = ImageIO.read(getClass().getResource("resources/resources/32/icons8_Download_from_the_Cloud_32px.png"));
            leftBarLabel1.setIcon(new ImageIcon(img));
            img = ImageIO.read(getClass().getResource("resources/resources/32/icons8_Approval_32px.png"));
            leftBarLabel2.setIcon(new ImageIcon(img));
            img = ImageIO.read(getClass().getResource("resources/resources/32/icons8_Clock_32px.png"));
            leftBarLabel3.setIcon(new ImageIcon(img));
            img = ImageIO.read(getClass().getResource("resources/resources/32/icons8-recycle-32.png"));
            restoreBtn.setIcon(new ImageIcon(img));
            img = ImageIO.read(getClass().getResource("resources/resources/logo/MM.png"));
            JLabel label = new JLabel();
            label.setIcon(new ImageIcon(img));
            label.setHorizontalAlignment(JLabel.CENTER);
            leftBar.add(leftBarPanel, BorderLayout.NORTH);
            leftBarPanel.add(label);
        } catch (Exception ex) {
            System.out.println(ex);
        }

        leftBarPanel.add(innerLeftBarPanel);

        for (JButton button : leftBarLabels) {
            button.setFont(Fonts.UbuntuFont);
            innerLeftBarPanel.add(button);
            button.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 10));
            button.setBackground(Colors.leftBarBackground);
            button.setFocusPainted(false);
            button.getParent().setBackground(Colors.leftBarBackground);
            button.setForeground(Colors.leftBarText);
            button.addMouseListener(new ButtonsAdaptor());
        }

        //Download Panel

        JScrollPane scrollPane = new JScrollPane(downloadPanel);
        scrollPane.setBorder(new EmptyBorder(0, 0, 0, 0));
        downloadInfoPanel = new JPanel();
        downloadInnerPanel = new JPanel();
        rightPanel.add(scrollPane, BorderLayout.CENTER);
        downloadPanel.setBackground(Colors.downloadBarBackground);
        selectedDownloads = new ArrayList<>();

        /*for (int i = 0; i < 10; i++) {
            Download download = new Download("1", "1");
            download.setSize(100);
            downloads.add(download);
            showInfo.add(i, false);
            //gl.setRows(gl.getRows() + 1);
            if (i % 3 == 0)
                download.setStatus("Processing");
            else if (i % 3 == 1)
                download.setStatus("Completed");
            else if (i % 3 == 2) {
                download.setStatus("Queues");
                queue.add(download);
            }
            download.setFileName(Integer.toString(i));
        }*/

        downloadInnerPanel = rightPanelMaker(leftBarSelected);
        downloadPanel.add(downloadInnerPanel, BorderLayout.NORTH);

        //Menu Bar

        downloadMenu.setFont(Fonts.UbuntuFont);
        helpMenu.setFont(Fonts.UbuntuFont);
        exit.setFont(Fonts.UbuntuFont);
        downloadMenu.setMnemonic(KeyEvent.VK_D);
        helpMenu.setMnemonic(KeyEvent.VK_H);
        exit.setMnemonic(KeyEvent.VK_E);
        frame.setJMenuBar(menuBar);
        menuBar.add(downloadMenu);
        menuBar.add(helpMenu);
        menuBar.add(exit);

        JMenuItem newDownload = new JMenuItem("New Download");
        newDownload.setAccelerator(KeyStroke.getKeyStroke("control N"));
        JMenuItem pause = new JMenuItem("Pause Download");
        pause.setAccelerator(KeyStroke.getKeyStroke("control P"));
        JMenuItem resume = new JMenuItem("Resume Download");
        resume.setAccelerator(KeyStroke.getKeyStroke("control R"));
        JMenuItem cancel = new JMenuItem("Cancel Download");
        cancel.setAccelerator(KeyStroke.getKeyStroke("control C"));
        JMenuItem remove = new JMenuItem("Remove Download");
        remove.setAccelerator(KeyStroke.getKeyStroke("control D"));
        JMenuItem settings = new JMenuItem("Settings");
        settings.setAccelerator(KeyStroke.getKeyStroke("control S"));
        JMenuItem export = new JMenuItem("Export");
        export.setAccelerator(KeyStroke.getKeyStroke("control E"));

        JMenuItem about = new JMenuItem("About Me");
        helpMenu.add(about);
        about.setFont(Fonts.UbuntuFont);
        about.setName(about.getText());
        about.addMouseListener(new ButtonsAdaptor());

        try {
            Image img = ImageIO.read(getClass().getResource("/resources/resources/16/icons8_Add_16px.png"));
            newDownload.setIcon(new ImageIcon(img));
            img = ImageIO.read(getClass().getResource("/resources/resources/16/icons8_Pause_Button_16px.png"));
            pause.setIcon(new ImageIcon(img));
            img = ImageIO.read(getClass().getResource("/resources/resources/16/icons8_Circled_Play_16px.png"));
            resume.setIcon(new ImageIcon(img));
            img = ImageIO.read(getClass().getResource("/resources/resources/16/icons8_Cancel_16px.png"));
            cancel.setIcon(new ImageIcon(img));
            img = ImageIO.read(getClass().getResource("/resources/resources/16/icons8_Trash_16px.png"));
            remove.setIcon(new ImageIcon(img));
            img = ImageIO.read(getClass().getResource("/resources/resources/16/icons8_Settings_16px.png"));
            settings.setIcon(new ImageIcon(img));
            img = ImageIO.read(getClass().getResource("/resources/resources/16/icons8-zip-16.png"));
            export.setIcon(new ImageIcon(img));
            img = ImageIO.read(getClass().getResource("/resources/resources/16/icons8-exit-16.png"));
            exit.setIcon(new ImageIcon(img));
            img = ImageIO.read(getClass().getResource("/resources/resources/16/icons8-help-16.png"));
            helpMenu.setIcon(new ImageIcon(img));
            img = ImageIO.read(getClass().getResource("/resources/resources/16/icons8-downloading-updates-16.png"));
            downloadMenu.setIcon(new ImageIcon(img));
            img = ImageIO.read(getClass().getResource("/resources/resources/16/icons8-user-16.png"));
            about.setIcon(new ImageIcon(img));
        } catch (Exception e) {
            System.out.println(e);
        }

        ArrayList<JMenuItem> downloadMenuItems = new ArrayList<>();
        downloadMenuItems.add(newDownload);
        downloadMenuItems.add(pause);
        downloadMenuItems.add(resume);
        downloadMenuItems.add(cancel);
        downloadMenuItems.add(remove);
        downloadMenuItems.add(settings);
        downloadMenuItems.add(export);

//        exit.addKeyListener(new KeyAdapter() {
//            @Override
//            public void keyPressed(KeyEvent keyEvent) {
//                super.keyPressed(keyEvent);
//                if (keyEvent.isControlDown() && keyEvent.getKeyChar() == 'e') {
//                    System.out.println("I'm Here :)");
//                    System.exit(0);
//                }
//            }
//        });

        exit.setName(exit.getText());
        exit.addMouseListener(new ButtonsAdaptor());

        for (JMenuItem menuItem : downloadMenuItems) {
            menuItem.setFont(Fonts.UbuntuFont);
            downloadMenu.add(menuItem);
            menuItem.setName(menuItem.getText());
            menuItem.addMouseListener(new ButtonsAdaptor());
        }

    }

    /**
     * sets the frame visible
     */
    public void setVisible() {
        //Display the window.
        //frame.pack();
        frame.setVisible(true);
    }

    /**
     * creates settings frame
     */
    private void settingsFrame() {
        JFrame settingsFrame = new JFrame("Settings");
        settingsFrame.setSize(400, 600);
        settingsFrame.setVisible(true);

        JPanel settingsFramePanel = new JPanel(new BorderLayout());
        settingsFramePanel.setBackground(Colors.newFramePanelBackground);
        settingsFrame.setContentPane(settingsFramePanel);

        settingsFramePanel.setBorder(new EmptyBorder(10, 10, 10, 10));

        JPanel simultaneouslyDownload = new JPanel(new BorderLayout());
        simultaneouslyDownload.setBackground(Colors.newFramePanelBackground);

        JLabel simultaneouslyLabel = new JLabel("Limitation for Simultaneous Downloads");
        simultaneouslyDownload.add(simultaneouslyLabel, BorderLayout.WEST);
        simultaneouslyLabel.setFont(Fonts.UbuntuFont);
        simultaneouslyLabel.setForeground(Colors.leftBarText);

        JCheckBox isSimultaneously = new JCheckBox();
        JSpinner spinner = new JSpinner();
        if (limitForSimultaneouslyDownload == -1)
            isSimultaneously.setSelected(false);
        else {
            isSimultaneously.setSelected(true);
            simultaneouslyDownload.add(spinner, BorderLayout.SOUTH);
            spinner.setValue(limitForSimultaneouslyDownload);
            settingsFrame.revalidate();
        }
        isSimultaneously.setContentAreaFilled(false);
        simultaneouslyDownload.add(isSimultaneously, BorderLayout.CENTER);
        isSimultaneously.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent itemEvent) {
                if (isSimultaneously.isSelected()) {
                    simultaneouslyDownload.add(spinner, BorderLayout.SOUTH);
                    spinner.setValue(limitForSimultaneouslyDownload);
                    settingsFrame.revalidate();
                } else {
                    simultaneouslyDownload.remove(spinner);
                    settingsFrame.revalidate();
                    limitForSimultaneouslyDownload = -1;
                }
            }
        });

        spinner.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                limitForSimultaneouslyDownload = (int) spinner.getValue();
                System.out.println(limitForSimultaneouslyDownload);
            }
        });
        simultaneouslyDownload.setBorder(new EmptyBorder(10, 10, 10, 10));
        settingsFramePanel.add(simultaneouslyDownload, BorderLayout.NORTH);

        JPanel selectLookAndFeel = new JPanel(new GridLayout(5, 1));
        selectLookAndFeel.setBackground(Colors.newFramePanelBackground);
        JLabel titledBorder = new JLabel("Look And Feel");
        titledBorder.setBorder(new EmptyBorder(10, 10, 10, 10));
        titledBorder.setForeground(Colors.leftBarText);
        titledBorder.setFont(Fonts.UbuntuFont);
        selectLookAndFeel.add(titledBorder);
        ButtonGroup group = new ButtonGroup();
        for (UIManager.LookAndFeelInfo each : UIManager.getInstalledLookAndFeels()) {
            JRadioButton option = new JRadioButton(each.getName());
            option.setFont(Fonts.UbuntuFont);
            option.setForeground(Colors.leftBarText);
            option.setBackground(Colors.newFramePanelBackground);
            option.setFocusPainted(true);
            group.add(option);
            option.setBorder(new EmptyBorder(5, 5, 5, 5));
            option.addActionListener(new RadioButtonActionListener());
            selectLookAndFeel.add(option);
        }
        selectLookAndFeel.setBorder(new LineBorder(Colors.leftBarText));
        settingsFramePanel.add(selectLookAndFeel, BorderLayout.CENTER);

        JPanel saveLocationChoose = new JPanel(new BorderLayout());
        saveLocationChoose.setBackground(Colors.newFramePanelBackground);
        JLabel locationText = new JLabel("Save in:");
        JButton saveLocation = new JButton("Save Path");
        locationText.setFont(Fonts.UbuntuFont);
        saveLocation.setFont(Fonts.UbuntuFont);
        locationText.setForeground(Colors.leftBarText);
        saveLocation.setFocusPainted(false);
        saveLocation.setBackground(Colors.newFramePanelBackground);
        saveLocation.setForeground(Colors.leftBarText);
        saveLocationChoose.add(locationText, BorderLayout.WEST);
        saveLocationChoose.add(saveLocation, BorderLayout.SOUTH);
        JFileChooser chooser = new JFileChooser();
        chooser.setMultiSelectionEnabled(false);
        chooser.setBackground(Colors.newFramePanelBackground);
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

        saveLocation.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent mouseEvent) {
                super.mousePressed(mouseEvent);
                defaultPath = chooser.getSelectedFile().getPath();
                System.out.println("default path changed");
            }
        });

        saveLocationChoose.add(chooser, BorderLayout.CENTER);
        saveLocationChoose.setBorder(new EmptyBorder(10, 10, 10, 10));

        JPanel filterEdit = new JPanel(new BorderLayout());
        JLabel filterText = new JLabel("Filter:");
        filterText.setFont(Fonts.UbuntuFont);

        filterText.setForeground(Colors.leftBarText);
        filterEdit.setBackground(Colors.newFramePanelBackground);
        JTextField filter = new JTextField("");

        JPanel filterAction = new JPanel(new GridLayout(1, 2));
        JButton addFilter = new JButton("Add Filter");
        addFilter.setName(addFilter.getText());
        addFilter.setBackground(Colors.newFramePanelBackground);
        addFilter.setFont(Fonts.UbuntuFont);
        addFilter.setForeground(Colors.leftBarText);

        JButton removeFilter = new JButton("Remove Filter");
        removeFilter.setName(addFilter.getText());
        removeFilter.setBackground(Colors.newFramePanelBackground);
        removeFilter.setFont(Fonts.UbuntuFont);
        removeFilter.setForeground(Colors.leftBarText);

        filterEdit.add(filter, BorderLayout.CENTER);
        filterEdit.add(filterText, BorderLayout.WEST);
        filterEdit.add(filterAction, BorderLayout.SOUTH);
        filterAction.add(addFilter);
        filterAction.add(removeFilter);

        JPanel southPanel = new JPanel(new BorderLayout());
        southPanel.add(saveLocationChoose, BorderLayout.NORTH);
        southPanel.add(filterEdit, BorderLayout.SOUTH);
        filterEdit.setBorder(new EmptyBorder(10, 10, 10, 10));
        settingsFramePanel.add(southPanel, BorderLayout.SOUTH);

        addFilter.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent mouseEvent) {
                super.mousePressed(mouseEvent);
                if (!filters.contains(filter.getText())) {
                    filters.add(filter.getText());
                    filter.setForeground(Color.black);
                    System.out.println("filter added " + filter.getText());
                } else {
                    filter.setText("Filter Has Been Added!");
                    filter.setForeground(Color.red);
                }
            }
        });
        removeFilter.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent mouseEvent) {
                super.mousePressed(mouseEvent);
                if (filters.contains(filter.getText())) {
                    filters.remove(filter.getText());
                    filter.setForeground(Color.black);
                    System.out.println("filter removed " + filter.getText());
                } else {
                    filter.setText("Filter Doesn't existed!");
                    filter.setForeground(Color.red);
                }
            }
        });

    }

    /**
     * creates about me frame
     */
    private void aboutFrame() {
        JFrame aboutFrame = new JFrame("About Me!");
        aboutFrame.setLocationRelativeTo(null);
        aboutFrame.setSize(350, 250);
        aboutFrame.setVisible(true);

        JPanel aboutFramePanel = new JPanel();
        aboutFrame.setContentPane(aboutFramePanel);

        GridLayout gl = new GridLayout(1, 1);
        JPanel grid = new JPanel(gl);
        grid.setBackground(Colors.aboutMeBackground);
        aboutFramePanel.setBackground(Colors.aboutMeBackground);
        aboutFramePanel.add(grid);
        grid.setBorder(new EmptyBorder(0, 0, 0, 0));

        ArrayList<JLabel> labels = new ArrayList<>();
        JLabel productName = new JLabel("MDownloadManager");
        JLabel name = new JLabel("Author : Mahdi Sadeghzadeh Ghamsary");
        JLabel studentNum = new JLabel("Student Num : 963544");
        JLabel startTime = new JLabel("Start Time : 13 May 1018");
        JLabel finishTime = new JLabel("Finish Time : 18 May 1018");
        JLabel description = new JLabel("Description : this is download manager :)");
        labels.add(productName);
        labels.add(name);
        labels.add(studentNum);
        labels.add(startTime);
        labels.add(finishTime);
        labels.add(description);
        gl.setRows(labels.size());

        for (JLabel label : labels) {
            label.setFont(Fonts.UbuntuFont);
            label.setForeground(Colors.leftBarText);
            label.setAlignmentX(Component.CENTER_ALIGNMENT);
            label.setBorder(new EmptyBorder(5, 5, 5, 5));
            grid.add(label);
        }
    }

    /**
     * creates new download frame
     */
    private void newDownloadFrame() {
        JFrame newDownloadFrame = new JFrame("New Downlaod");
        newDownloadFrame.setSize(500, 500);
        newDownloadFrame.setVisible(true);
        newDownloadFrame.setBackground(Colors.newFramePanelBackground);

        JPanel newDownloadFramePanel = new JPanel(new BorderLayout());
        newDownloadFramePanel.setBackground(Colors.newFramePanelBackground);
        newDownloadFrame.setContentPane(newDownloadFramePanel);
        JPanel labels = new JPanel(new BorderLayout());
        labels.setBackground(Colors.newFramePanelBackground);
        JPanel content = new JPanel(new BorderLayout());
        content.setBackground(Colors.newFramePanelBackground);
        newDownloadFramePanel.setBorder(new EmptyBorder(10, 10, 10, 10));

        JTextField link = new JTextField();
        JTextField fileName = new JTextField();
        fileName.setFont(Fonts.UbuntuFont);

        JLabel linkText = new JLabel("Link:");
        linkText.setFont(Fonts.UbuntuFont);
        linkText.setForeground(Colors.leftBarText);
        JLabel fileText = new JLabel("Name:");
        fileText.setFont(Fonts.UbuntuFont);
        fileText.setForeground(Colors.leftBarText);
        linkText.setBorder(new EmptyBorder(0, 10, 0, 10));
        fileText.setBorder(new EmptyBorder(5, 10, 0, 10));

        JButton addDownlaod = new JButton("Add Download");
        addDownlaod.setFont(Fonts.UbuntuFont);
        addDownlaod.setForeground(Colors.leftBarText);
        addDownlaod.setBackground(Colors.newFramePanelBackground);
        addDownlaod.setFocusPainted(false);

        GridLayout gl = new GridLayout(2, 1);
        gl.setVgap(5);
        JPanel innerContent = new JPanel(gl);
        innerContent.setBackground(Colors.newFramePanelBackground);
        innerContent.setBorder(new EmptyBorder(0, 0, 0, 0));
        JPanel innerLabels = new JPanel(new GridLayout(2, 1));
        innerLabels.setBackground(Colors.newFramePanelBackground);

        JPanel chooserPanel = new JPanel(new BorderLayout());
        JLabel locationText = new JLabel("Save in:");
        locationText.setFont(Fonts.UbuntuFont);
        locationText.setForeground(Colors.leftBarText);
        JFileChooser chooser = new JFileChooser();
        chooser.setMultiSelectionEnabled(false);
        chooserPanel.setBackground(Colors.newFramePanelBackground);
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooserPanel.add(chooser, BorderLayout.CENTER);
        chooserPanel.add(locationText, BorderLayout.WEST);
//        chooser.setBorder(new EmptyBorder(5,5,5,5));

//        String[] downloadStartMode = new String[1];
        JPanel selectDwonloadStartMode = new JPanel(new GridLayout(5, 1));
        selectDwonloadStartMode.setBackground(Colors.newFramePanelBackground);
        JLabel titledBorder = new JLabel("Start with");
        titledBorder.setBorder(new EmptyBorder(5, 5, 5, 5));
        titledBorder.setForeground(Colors.leftBarText);
        titledBorder.setFont(Fonts.UbuntuFont);
        selectDwonloadStartMode.add(titledBorder);
        ButtonGroup group = new ButtonGroup();
        ArrayList<JRadioButton> radioButtons = new ArrayList<>();
        JRadioButton automatically = new JRadioButton("Automatically");
        JRadioButton manually = new JRadioButton("Manually");
        JRadioButton queue1 = new JRadioButton("Queue");
        radioButtons.add(automatically);
        radioButtons.add(manually);
        radioButtons.add(queue1);
        automatically.setSelected(true);
        for (JRadioButton radioButton : radioButtons) {
            radioButton.setFont(Fonts.UbuntuFont);
            radioButton.setForeground(Colors.leftBarText);
            radioButton.setBackground(Colors.newFramePanelBackground);
            radioButton.setFocusPainted(false);
            group.add(radioButton);
            radioButton.setBorder(new EmptyBorder(5, 5, 5, 5));
            selectDwonloadStartMode.add(radioButton);
        }
        selectDwonloadStartMode.setBorder(new LineBorder(Colors.leftBarText));

        JPanel centerPanel = new JPanel(new BorderLayout());
        centerPanel.add(selectDwonloadStartMode, BorderLayout.NORTH);
        centerPanel.add(chooserPanel, BorderLayout.CENTER);
        centerPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
        centerPanel.setBackground(Colors.newFramePanelBackground);

        JPanel topPanel = new JPanel(new BorderLayout());
        topPanel.add(labels, BorderLayout.WEST);
        topPanel.add(content, BorderLayout.CENTER);
        content.add(innerContent, BorderLayout.NORTH);
        labels.add(innerLabels, BorderLayout.NORTH);
        innerLabels.add(linkText);
        innerContent.add(link);
        innerLabels.add(fileText);
        innerContent.add(fileName);
        newDownloadFramePanel.add(topPanel, BorderLayout.NORTH);
        newDownloadFramePanel.add(centerPanel, BorderLayout.CENTER);
        newDownloadFramePanel.add(addDownlaod, BorderLayout.SOUTH);

        addDownlaod.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent mouseEvent) {
                super.mousePressed(mouseEvent);
                Download download;
                boolean isValid = true;
                System.out.println(filters.size());
                for (String filter : filters) {
                    if (link.getText().indexOf(filter) == 0) {
                        isValid = false;
                        gl.setRows(5);
                        for (int i = 0; i < innerContent.getComponentCount(); i++) {
                            if (innerContent.getComponent(i) instanceof JLabel)
                                if (((JLabel) innerContent.getComponent(i)).getText().equals("*Couldn't Add This Download. It's Name Or Address Was Filtered"))
                                    innerContent.remove(innerContent.getComponent(i));
                        }
                        JLabel err = new JLabel("Couldn't Add This Download. It's Name Or Address Was Filtered");
                        err.setFont(Fonts.UbuntuFont);
                        err.setForeground(Color.red);
                        innerContent.add(err);
                        newDownloadFrame.revalidate();
                        break;
                    } else if (fileName.getText().equals("") || link.getText().equals("")) {
                        isValid = false;
                        gl.setRows(5);
                        for (int i = 0; i < innerContent.getComponentCount(); i++) {
                            if (innerContent.getComponent(i) instanceof JLabel)
                                if (((JLabel) innerContent.getComponent(i)).getText().equals("*TextFields Must Be Initialized!"))
                                    innerContent.remove(innerContent.getComponent(i));
                        }
                        JLabel err = new JLabel("*TextFields Must Be Initialized!");
                        err.setFont(Fonts.UbuntuFont);
                        err.setForeground(Color.red);
                        innerContent.add(err);
                        newDownloadFrame.revalidate();
                    }
                }
                if (isValid) {
                    download = new Download(fileName.getText(), link.getText(), returnGUI());
                    if (chooser.getSelectedFile() == null)
                        download.setSaveLocation(defaultPath);
                    else
                        download.setSaveLocation(chooser.getSelectedFile().getPath());
                    if (automatically.isSelected()) {
                        DownloadManager.startDownloading(download);
                    } else if (queue1.isSelected()) {
                        queue.add(download);
                        queueShowInfo.add(false);
                    }
                    download.setStatus("Processing");
                    try {
                        URL url = new URL(download.getLink());
                        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                        connection.setRequestMethod("GET");
                        download.setSize(Float.valueOf(connection.getHeaderField("Content-length")) / 1024);
                        connection.disconnect();
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                    downloads.add(download);
                    showInfo.add(false);
                    downloadPanel.remove(downloadInnerPanel);
                    downloadPanel.repaint();
                    downloadInnerPanel = rightPanelMaker(leftBarSelected);
                    downloadPanel.add(downloadInnerPanel, BorderLayout.NORTH);
                    frame.revalidate();
                    newDownloadFrame.dispose();
                }
            }
        });

    }

    /**
     * shows info of a download
     * @param download
     * @return the panel to add into frame
     */
    private JPanel getDownloadInfo(Download download) {

        JPanel downloadInfoFramePanel = new JPanel(new BorderLayout());
//        downloadInfoFramePanel.setMaximumSize(new Dimension(300,300));
        downloadInfoFramePanel.setBackground(Colors.infoPanelBackground);
        JPanel labels = new JPanel(new BorderLayout());
        ArrayList<JLabel> label = new ArrayList<>();
        labels.setBackground(Colors.infoPanelBackground);
        labels.setBorder(new EmptyBorder(25, 25, 25, 25));
        downloadInfoFramePanel.add(labels, BorderLayout.EAST);

        JPanel innerLabels = new JPanel(new GridLayout(9, 2));
        innerLabels.setBackground(Colors.infoPanelBackground);
        innerLabels.setBorder(new EmptyBorder(10, 0, 10, 0));

        JLabel fileNameLabel = new JLabel("File:");
        label.add(fileNameLabel);
        JLabel fileName = new JLabel(download.getFileName());
        label.add(fileName);

        JLabel statusLabel = new JLabel("Status:");
        label.add(statusLabel);

        JLabel status = new JLabel(download.getStatus());
        label.add(status);

        JLabel sizeLabel = new JLabel("Size:");
        label.add(sizeLabel);

        JLabel size = new JLabel("" + download.getSize()/(float) 1024);
        label.add(size);

        JLabel saveToLabel = new JLabel("Save To:");
        label.add(saveToLabel);

        JLabel saveTo = new JLabel(download.getSaveLocation());
        label.add(saveTo);

        JLabel createLabel = new JLabel("Created:");
        label.add(createLabel);

        JLabel create = new JLabel("" + download.getCreateTime());
        label.add(create);

        JLabel finishLabel = new JLabel("Finished:");
        label.add(finishLabel);

        JLabel finish = new JLabel("" + download.getFinishTime());
        label.add(finish);

//        JLabel linkLabel = new JLabel("URL:");
//        label.add(linkLabel);
//
//        JLabel link = new JLabel(download.getLink());
//        label.add(link);

        for (JLabel jLabel : label) {
            jLabel.setFont(Fonts.UbuntuFont);
            jLabel.setHorizontalAlignment(Label.LEFT);
            jLabel.setBorder(new EmptyBorder(1, 0, 10, 0));
            innerLabels.add(jLabel);
        }

        labels.add(innerLabels, BorderLayout.NORTH);

        if (queue.contains(download)) {
            JButton add = new JButton("remove from queue");
            add.setBackground(Colors.infoPanelBackground);
            add.setFont(Fonts.UbuntuFont);
            add.setName("removeFromQueue");
            add.addMouseListener(new ButtonsAdaptor());

            JButton startQueue = new JButton("Start/Stop Queue");
            startQueue.setBackground(Colors.infoPanelBackground);
            startQueue.setFont(Fonts.UbuntuFont);
            startQueue.setName("startQueue");
            startQueue.addMouseListener(new ButtonsAdaptor());

            JButton moveUp = new JButton();
            JButton moveDown = new JButton();
            moveDown.setName("Move Down");
            moveUp.setName("Move Up");
            moveDown.setBackground(Colors.infoPanelBackground);
            moveUp.setBackground(Colors.infoPanelBackground);
            innerLabels.add(moveUp);
            innerLabels.add(add);
            innerLabels.add(moveDown);
            innerLabels.add(startQueue);
            try {
                Image img = ImageIO.read(getClass().getResource("resources/resources/32/icons8-sort-down-32.png"));
                moveDown.setIcon(new ImageIcon(img));
                img = ImageIO.read(getClass().getResource("resources/resources/32/icons8-sort-up-32.png"));
                moveUp.setIcon(new ImageIcon(img));
            } catch (Exception e) {
                System.out.println(e);
            }
            moveDown.addMouseListener(new ButtonsAdaptor());
            moveUp.addMouseListener(new ButtonsAdaptor());
        } else if (removed.contains(download)) {
            JButton add = new JButton("Restore");
            add.setBackground(Colors.infoPanelBackground);
            add.setFont(Fonts.UbuntuFont);
            add.setName("restoreDownload");
            add.addMouseListener(new ButtonsAdaptor());
            innerLabels.add(add);
        } else if (downloads.contains(download)) {
            JButton add = new JButton("add to queue");
            add.setBackground(Colors.infoPanelBackground);
            add.setFont(Fonts.UbuntuFont);
            add.setName("addToQueue");
            add.addMouseListener(new ButtonsAdaptor());
            innerLabels.add(add);
        }

        return downloadInfoFramePanel;
    }

    /**
     * makes the input download panel
     * @param download
     * @return the panel to add into frame
     */
    private JPanel downloadPanelMaker(Download download) {
        JPanel panel = new JPanel(new BorderLayout());
        panel.setBorder(new EmptyBorder(10, 10, 10, 10));
        panel.setBackground(Colors.downloadBarBackground);
        ArrayList<JComponent> componenets = new ArrayList<>();

        JPanel centerPanel = new JPanel(new BorderLayout());
        centerPanel.setBackground(Colors.downloadBarBackground);
        centerPanel.setBorder(new EmptyBorder(0, 0, 0, 0));

        JLabel name = new JLabel("Name : " + download.getFileName());
        name.setBorder(new EmptyBorder(0, 10, 10, 0));
        name.setFont(Fonts.UbuntuFont);
        name.setForeground(Colors.DownloadBarText);
        //System.out.println(download.getFileName());

        JProgressBar progressBar = new JProgressBar();
        //progressBar.setBorder(new EmptyBorder(10,10,0,0));
        progressBar.setStringPainted(true);
        progressBar.setBackground(Colors.DownloadBarText);
        //progressBar.set
        progressBar.setMinimum(0);
        progressBar.setMaximum((int) download.getSize());
        //progressBar.setIndeterminate(true);
        //progressBar.setOrientation(1);
        progressBar.setValue((int) (download.getProgress()));
        progressBar.setForeground(Colors.progressBar);
        progressBar.setFont(Fonts.UbuntuFont);

        JButton infoBtn = new JButton();
        infoBtn.setFocusPainted(false);
        infoBtn.setContentAreaFilled(false);
        infoBtn.setBorder(new EmptyBorder(0, 0, 0, 0));
        componenets.add(infoBtn);
        infoBtn.setName("infoBtn");
        infoBtn.addMouseListener(new ButtonsAdaptor());
        try {
            Image img = ImageIO.read(getClass().getResource("/resources/resources/64/icons8-chevron-right-48.png"));
            infoBtn.setIcon(new ImageIcon(img));
        } catch (Exception e) {
            System.out.println(e);
        }

        centerPanel.add(name, BorderLayout.NORTH);
        centerPanel.add(progressBar, BorderLayout.CENTER);
        panel.add(infoBtn, BorderLayout.EAST);
        panel.add(centerPanel, BorderLayout.CENTER);

        JLabel size = new JLabel(Float.toString(rounder(download.getSpeed() / (float) 1024)) + " MB/s     " + Float.toString(rounder(download.getProgress() / 1024)) + "MB/" + Float.toString(rounder(download.getSize() / 1024)) + "MB");
        size.setBorder(new EmptyBorder(0, 0, 0, 10));
        size.setFont(Fonts.UbuntuFont);
        size.setForeground(Colors.DownloadBarText);

        JPanel bottomPanel = new JPanel(new BorderLayout());
        bottomPanel.setBackground(Colors.downloadBarBackground);
        bottomPanel.setBorder(new EmptyBorder(10, 0, 0, 0));
        JPanel icons = new JPanel(new GridLayout(1, 3, 10, 10));
        icons.setBorder(new EmptyBorder(0, 10, 0, 0));
        icons.setBackground(Colors.downloadBarBackground);
        ArrayList<JButton> iconButtons = new ArrayList<>();
        JButton resumeBtn = new JButton();
        resumeBtn.setName("resumeBtn");
        JButton pauseBtn = new JButton();
        pauseBtn.setName("pauseBtn");
        JButton folderBtn = new JButton();
        folderBtn.setName("folderBtn");
        iconButtons.add(resumeBtn);
        iconButtons.add(pauseBtn);
        iconButtons.add(folderBtn);
        componenets.add(resumeBtn);
        componenets.add(pauseBtn);
        componenets.add(folderBtn);
        componenets.add(icons);
        try {
            Image img = ImageIO.read(getClass().getResource("/resources/resources/16/icons8-circled-play-16.png"));
            resumeBtn.setIcon(new ImageIcon(img));
            img = ImageIO.read(getClass().getResource("/resources/resources/16/icons8-pause-button-16.png"));
            pauseBtn.setIcon(new ImageIcon(img));
            img = ImageIO.read(getClass().getResource("/resources/resources/16/icons8-folder-16.png"));
            folderBtn.setIcon(new ImageIcon(img));
            for (JButton button : iconButtons) {
                button.setContentAreaFilled(false);
                button.setFocusPainted(false);
                button.setBorder(new EmptyBorder(0, 5, 0, 5));
                icons.add(button);
                button.addMouseListener(new ButtonsAdaptor());
            }
        } catch (Exception e) {
            System.out.println(e);
        }

        panel.add(bottomPanel, BorderLayout.SOUTH);
        bottomPanel.add(icons, BorderLayout.WEST);
        bottomPanel.add(size, BorderLayout.EAST);

        componenets.add(panel);
        for (int i = 0; i < panel.getComponentCount(); i++)
            componenets.add((JComponent) panel.getComponent(i));

        panel.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent mouseEvent) {
                super.mouseReleased(mouseEvent);
                panel.setBackground(Colors.downloadBarOnClick);
                if (selectedDownloads.contains(download))
                    selectedDownloads.remove(download);
                else
                    selectedDownloads.add(download);
                for (JComponent obj : componenets) {
                    obj.setBackground(Colors.downloadBarOnClick);
                }
                if (mouseEvent.getClickCount() == 2) {
                    Desktop desktop;
                    if (!Desktop.isDesktopSupported()) {
                        System.out.println("desktop is not supported");
                        return;
                    }
                    desktop = Desktop.getDesktop();
                    String path = focusedDownload.getSaveLocation() + "/" + focusedDownload.getFileName();
                    //System.out.println(System.getProperty("user.dir"));
                    try {
                        File fPath = new File(path);
                        if (!fPath.exists()) {
                            System.out.println("path nist");
                        }
                        if (!fPath.isDirectory()) {
                            System.out.println("dir nist");
                        }
                        if (download.getStatus().equals("Completed"))
                            desktop.open(new File(path));
                    } catch (IOException e) {
                        System.out.println(e);
                        e.printStackTrace();
                    }
                } else if (SwingUtilities.isRightMouseButton(mouseEvent)) {
                    if (!showInfo.get(downloads.indexOf(focusedDownload))) {
                        for (int i = 0; i < showInfo.size(); i++) {
                            if (showInfo.get(i))
                                rightPanel.remove(downloadInfoPanel);
                            showInfo.set(i, false);
                        }
                        downloadInfoPanel = getDownloadInfo(focusedDownload);
                        rightPanel.add(downloadInfoPanel, BorderLayout.EAST);
                        showInfo.set(downloads.indexOf(focusedDownload), true);
                    } else {
                        rightPanel.remove(downloadInfoPanel);
                        showInfo.set(downloads.indexOf(focusedDownload), false);
                    }
                    frame.revalidate();
                }
            }

            @Override
            public void mouseReleased(MouseEvent mouseEvent) {
                super.mouseReleased(mouseEvent);
//                mouseEntered(mouseEvent);
            }

            public void mouseEntered(MouseEvent e) {
                focusedDownload = download;
                if (!selectedDownloads.contains(download))
                    for (JComponent obj : componenets) {
                        obj.setBackground(Colors.downloadBarHover);
                        if (obj instanceof JButton) {
                            ((JButton) obj).setContentAreaFilled(true);
                        }
                    }
            }

            public void mouseExited(MouseEvent e) {
                if (!selectedDownloads.contains(download))
                    for (JComponent obj : componenets) {
                        obj.setBackground(Colors.downloadBarBackground);
                        if (obj instanceof JButton) {
                            ((JButton) obj).setContentAreaFilled(false);
                        }
                    }
            }

        });
        return panel;
    }

    /**
     * puts downloads on the right panel
     * @param leftBarSelected
     * @return the panel to add into frame
     */
    private JPanel rightPanelMaker(String leftBarSelected) {
        GridLayout gl = new GridLayout(0, 1);
        JPanel innerDownloadPanel = new JPanel(gl);
        innerDownloadPanel.setBackground(Colors.downloadPanelBackground);
        switch (leftBarSelected) {
            case "Queues":
                for (Download download : queue) {
                    gl.setRows(gl.getRows() + 1);
                    innerDownloadPanel.add(downloadPanelMaker(download));
                }
                break;
            case "Processing":
                for (Download download : downloads) {
                    if (download.getStatus().equals(leftBarSelected)) {
                        gl.setRows(gl.getRows() + 1);
                        innerDownloadPanel.add(downloadPanelMaker(download));
                    }
                }
                break;
            case "Completed":
                for (Download download : downloads) {
                    if (download.getStatus().equals(leftBarSelected)) {
                        gl.setRows(gl.getRows() + 1);
                        innerDownloadPanel.add(downloadPanelMaker(download));
                    }
                }
                break;
            case "Restore":
                for (Download download : removed) {
                    gl.setRows(gl.getRows() + 1);
                    innerDownloadPanel.add(downloadPanelMaker(download));
                }
                break;
            case "searchResult":
                for (Download download : searchResult) {
                    gl.setRows(gl.getRows() + 1);
                    innerDownloadPanel.add(downloadPanelMaker(download));
                }
                break;
            case "sortResult":
                for (Download download : sorted) {
                    gl.setRows(gl.getRows() + 1);
                    innerDownloadPanel.add(downloadPanelMaker(download));
                }
                break;
        }
        return innerDownloadPanel;
    }

    /**
     * sets the look and feel mode
     * @param lookAndFeel the string if look and feel
     */
    private void setLookAndFeel(String lookAndFeel) {
        this.lookAndFeel = lookAndFeel;
        try {
            UIManager.setLookAndFeel(lookAndFeel);
            SwingUtilities.updateComponentTreeUI(frame);
            frame.revalidate();
        } catch (Exception ignored) {
        }
    }

    /**
     * loads saved data
     */
    private void onLoad() {
        showInfo = new ArrayList<>();
        queueShowInfo = new ArrayList<>();
        restoreShowInfo = new ArrayList<>();
        sortBy = new ArrayList<>();
        searchResult = new ArrayList<>();
        isDescending = false;
        if ((new File("downloads.mdm")).exists()) {
            downloads = FileManager.<Download>load("downloads");
//            System.out.println(downloads);
            for (int i = 0; i < downloads.size(); i++)
                showInfo.add(false);
//            System.out.println(showInfo);
        } else
            downloads = new ArrayList<>();

        if ((new File("filters.mdm")).exists()) {
            filters = FileManager.<String>load("filters");
        } else
            filters = new ArrayList<>();

        if ((new File("queue.mdm")).exists()) {
            queue = FileManager.<Download>load("queue");
            for (int i = 0; i < queue.size(); i++)
                queueShowInfo.add(false);
//            System.out.println(queue);
        } else
            queue = new ArrayList<>();

        if ((new File("removed.mdm")).exists()) {
            removed = FileManager.<Download>load("removed");
            for (int i = 0; i < removed.size(); i++)
                restoreShowInfo.add(false);
        } else
            removed = new ArrayList<>();

        if ((new File("settings.mdm")).exists()) {
            lookAndFeel = FileManager.<String>load("settings").get(0);
            limitForSimultaneouslyDownload = Integer.valueOf(FileManager.<String>load("settings").get(1));
            defaultPath = FileManager.<String>load("settings").get(2);
        } else {
            lookAndFeel = "";
            limitForSimultaneouslyDownload = -1;
            defaultPath = "/home/mahdi/Desktop";
        }

    }

    /**
     * saves data
     */
    private void onExit() {
        FileManager.save(downloads, "downloads");
        FileManager.save(queue, "queue");
        FileManager.save(filters, "filters");
        ArrayList<String> settings = new ArrayList<>();
        settings.add(lookAndFeel);
        settings.add(Integer.toString(limitForSimultaneouslyDownload));
        settings.add(defaultPath);
        FileManager.save(settings, "settings");
        frame.dispose();
        System.exit(0);
    }

    /**
     * searchs in downloads
     * @param searchText
     */
    private void doSearch(String searchText) {
        System.out.println("doSearch");
        for (Download download : downloads) {
            if (download.getFileName().contains(searchText) || download.getLink().contains(searchText)) {
                searchResult.add(download);
            }
        }

        downloadPanel.remove(downloadInnerPanel);
        downloadPanel.repaint();
        downloadInnerPanel = rightPanelMaker("searchResult");
        downloadPanel.add(downloadInnerPanel, BorderLayout.NORTH);
        frame.revalidate();
        searchResult.clear();
    }

    /**
     * sorts downloads
     */
    private void doSort() {
        sorted = downloads;
        for (int i = sortBy.size() - 1; i >= 0; i--) {
            if (sortBy.get(i).equals("By Name"))
                Collections.sort(sorted, Download.byName);
            else if (sortBy.get(i).equals("By Size"))
                Collections.sort(sorted, Download.bySize);
            else if (sortBy.get(i).equals("By Start Time"))
                Collections.sort(sorted, Download.byTime);
        }
        if (isDescending)
            Collections.reverse(sorted);
        downloadPanel.remove(downloadInnerPanel);
        downloadPanel.repaint();
        downloadInnerPanel = rightPanelMaker("sortResult");
        downloadPanel.add(downloadInnerPanel, BorderLayout.NORTH);
        frame.revalidate();
    }

    /**
     * handlers classes
     */
    private class ButtonsAdaptor implements MouseListener {
        @Override
        public void mouseReleased(MouseEvent mouseEvent) {
//            if (mouseEvent.getComponent() instanceof JButton) {
            mouseEvent.getComponent().setBackground(mouseEvent.getComponent().getParent().getBackground().brighter());
//                System.out.println(mouseEvent.getComponent().getName());
            switch (mouseEvent.getComponent().getName()) {
                case "Pause Download":
                    for (Download download : selectedDownloads) {
                        DownloadManager.stopDownloading(download);
//                        System.out.println(download.getFileName() + " paused");
                    }
                    for (int i = 0; i < selectedDownloads.size(); i++)
                        selectedDownloads.remove(selectedDownloads.get(i));
                    break;
                case "Resume Download":
                    for (Download download : selectedDownloads) {
                        DownloadManager.startDownloading(download);
//                        System.out.println(download.getFileName() + " resumed");
                    }
                    for (int i = 0; i < selectedDownloads.size(); i++)
                        selectedDownloads.remove(selectedDownloads.get(i));
                    break;
                case "Remove Download":
                    for (Download download : selectedDownloads) {
                        if (download.isDownloading())
                            DownloadManager.stopDownloading(download);
                        System.out.println(download.getFileName() + " removed");
                        if (leftBarSelected.equals("Processing")) {
                            FileManager.delete(new File(download.getSaveLocation() + "/" + download.getFileName()));
                            removed.add(download);
                            if (queue.contains(download)) {
                                queueShowInfo.remove(queue.indexOf(download));
                                queue.remove(download);
                            }
                            download.setStatus("Restore");
                            showInfo.remove(downloads.indexOf(download));
                            downloads.remove(download);
                            download.setStatus("Restore");
                        } else if (leftBarSelected.equals("Queues")) {
                            queueShowInfo.remove(queue.indexOf(download));
                            queue.remove(download);
                        } else
                            removed.remove(download);
                    }
                    FileManager.save(removed, "removed");
                    for (int i = 0; i < selectedDownloads.size(); i++)
                        selectedDownloads.remove(selectedDownloads.get(i));
                    downloadPanel.remove(downloadInnerPanel);
                    downloadPanel.repaint();
                    downloadInnerPanel = rightPanelMaker(leftBarSelected);
                    downloadPanel.add(downloadInnerPanel, BorderLayout.NORTH);
                    frame.revalidate();
                    break;
                case "restoreDownload":
                    focusedDownload.setStatus("Processing");
                    downloadPanel.remove(downloadInnerPanel);
                    downloadPanel.repaint();
                    downloadInnerPanel = rightPanelMaker(leftBarSelected);
                    downloadPanel.add(downloadInnerPanel, BorderLayout.NORTH);
                    frame.revalidate();
                    break;
                case "Sort Downloads":
                    JPopupMenu popup = new JPopupMenu();
                    JCheckBoxMenuItem nameItem = new JCheckBoxMenuItem("By Name");
                    JCheckBoxMenuItem sizeItem = new JCheckBoxMenuItem("By Size");
                    JCheckBoxMenuItem timeItem = new JCheckBoxMenuItem("By Start Time");
                    JCheckBoxMenuItem descendingItem = new JCheckBoxMenuItem("Sort Descending");
                    nameItem.setName(nameItem.getText());
                    sizeItem.setName(sizeItem.getText());
                    timeItem.setName(timeItem.getText());
                    descendingItem.setName(descendingItem.getText());
                    nameItem.setFont(Fonts.UbuntuFont);
                    sizeItem.setFont(Fonts.UbuntuFont);
                    timeItem.setFont(Fonts.UbuntuFont);
                    descendingItem.setFont(Fonts.UbuntuFont);
                    descendingItem.setState(isDescending);
                    for (String str : sortBy) {
//                        System.out.println(str);
//                        System.out.println(descendingItem.getText());
                        if (str.equals("By Name")) {
                            nameItem.setState(true);
                        }
                        if (str.equals("By Size")) {
                            sizeItem.setState(true);
                        }
                        if (str.equals("By Start Time")) {
                            timeItem.setState(true);
                        }
                    }
                    nameItem.addMouseListener(new ButtonsAdaptor());
                    sizeItem.addMouseListener(new ButtonsAdaptor());
                    timeItem.addMouseListener(new ButtonsAdaptor());
                    descendingItem.addMouseListener(new ButtonsAdaptor());
                    popup.add(nameItem);
                    popup.add(sizeItem);
                    popup.add(timeItem);
                    popup.add(descendingItem);
                    popup.show(frame, mouseEvent.getComponent().getParent().getParent().getLocation().x + mouseEvent.getComponent().getParent().getLocation().x + mouseEvent.getComponent().getLocation().x + mouseEvent.getX(), mouseEvent.getComponent().getParent().getParent().getLocation().y + mouseEvent.getComponent().getParent().getLocation().y + mouseEvent.getComponent().getLocation().y + mouseEvent.getY());
                    break;
                case "Search in Downloads":
                    JDialog dialog = new JDialog(frame, "search");
                    dialog.setLocation(frame.getLocation().x + mouseEvent.getComponent().getParent().getParent().getLocation().x + mouseEvent.getComponent().getParent().getLocation().x + mouseEvent.getComponent().getLocation().x + mouseEvent.getX(), frame.getLocation().y + mouseEvent.getComponent().getParent().getParent().getLocation().y + mouseEvent.getComponent().getParent().getLocation().y + mouseEvent.getComponent().getLocation().y + mouseEvent.getY());
                    dialog.setSize(200, 20);
//                    dialog.setModal(true);
                    JPanel searchDialog = new JPanel(new BorderLayout());
                    dialog.setContentPane(searchDialog);
                    JTextField searchField = new JTextField();
                    JButton doSearch = new JButton("Search");
                    doSearch.setFont(Fonts.UbuntuFont);
                    doSearch.setBackground(Colors.newFramePanelBackground);
                    doSearch.setForeground(Colors.leftBarText);
                    searchDialog.add(searchField, BorderLayout.CENTER);
                    searchDialog.add(doSearch, BorderLayout.EAST);

                    doSearch.addMouseListener(new MouseAdapter() {
                        @Override
                        public void mousePressed(MouseEvent mouseEvent) {
                            super.mousePressed(mouseEvent);
                            if (!searchField.getText().isEmpty()) {
                                doSearch(searchField.getText());
                            } else {
                                searchField.setText("No Input To Search!");
                            }

                        }
                    });

                    dialog.setVisible(true);
                    break;
                case "Cancel Download":
                    for (Download download : selectedDownloads) {
                        download.setDownloading(false);
                        System.out.println(download.getFileName() + " canceled");
                    }
                    for (int i = 0; i < selectedDownloads.size(); i++)
                        selectedDownloads.remove(selectedDownloads.get(i));
                    break;
                case "About Me":
                    System.out.println(mouseEvent.getComponent().getName());
                    aboutFrame();
                    break;
                case "Exit":
                    onExit();
                    System.out.println("exit");
                    break;
                case "infoBtn":
                    if (leftBarSelected.equals("Processing")) {
                        if (!showInfo.get(downloads.indexOf(focusedDownload))) {
                            for (int i = 0; i < showInfo.size(); i++) {
                                if (showInfo.get(i))
                                    rightPanel.remove(downloadInfoPanel);
                                showInfo.set(i, false);
                            }
                            downloadInfoPanel = getDownloadInfo(focusedDownload);
                            rightPanel.add(downloadInfoPanel, BorderLayout.EAST);
                            showInfo.set(downloads.indexOf(focusedDownload), true);
                        } else {
                            rightPanel.remove(downloadInfoPanel);
                            showInfo.set(downloads.indexOf(focusedDownload), false);
                        }
                    } else if (leftBarSelected.equals("Queues")) {
                        if (!queueShowInfo.get(queue.indexOf(focusedDownload))) {
                            for (int i = 0; i < queueShowInfo.size(); i++) {
                                if (queueShowInfo.get(i))
                                    rightPanel.remove(downloadInfoPanel);
                                queueShowInfo.set(i, false);
                            }
                            downloadInfoPanel = getDownloadInfo(focusedDownload);
                            rightPanel.add(downloadInfoPanel, BorderLayout.EAST);
                            queueShowInfo.set(queue.indexOf(focusedDownload), true);
                        } else {
                            rightPanel.remove(downloadInfoPanel);
                            queueShowInfo.set(queue.indexOf(focusedDownload), false);
                        }
                    } else {
                        if (!restoreShowInfo.get(removed.indexOf(focusedDownload))) {
                            for (int i = 0; i < restoreShowInfo.size(); i++) {
                                if (restoreShowInfo.get(i))
                                    rightPanel.remove(downloadInfoPanel);
                                restoreShowInfo.set(i, false);
                            }
                            downloadInfoPanel = getDownloadInfo(focusedDownload);
                            rightPanel.add(downloadInfoPanel, BorderLayout.EAST);
                            restoreShowInfo.set(removed.indexOf(focusedDownload), true);
                        } else {
                            rightPanel.remove(downloadInfoPanel);
                            restoreShowInfo.set(removed.indexOf(focusedDownload), false);
                        }
                    }
                    frame.revalidate();
                    break;
                case "newDownloadBtn":
                case "New Download":
                    newDownloadFrame();
                    break;
                case "resumeBtn":
                    DownloadManager.startDownloading(focusedDownload);
                    System.out.println(focusedDownload.getFileName() + " resumed");
                    break;
                case "pauseBtn":
                    DownloadManager.stopDownloading(focusedDownload);
                    System.out.println(focusedDownload.getFileName() + " paused");
                    break;
                case "removeBtn":
                    System.out.println(focusedDownload.getFileName() + " removed");
                    if (!leftBarSelected.equals("Restore")) {
                        removed.add(focusedDownload);
                        focusedDownload.setStatus("Restore");
                    } else
                        removed.remove(focusedDownload);
                    FileManager.delete(new File(focusedDownload.getSaveLocation() + "/" + focusedDownload.getFileName()));
                    FileManager.save(removed, "removed");
                    downloads.remove(focusedDownload);
                    if (queue.contains(focusedDownload))
                        queue.remove(focusedDownload);
                    focusedDownload = null;
                    downloadPanel.repaint();
                    downloadInnerPanel = rightPanelMaker(leftBarSelected);
                    frame.revalidate();
                    break;
                case "cancelBtn":
                    System.out.println(focusedDownload.getFileName() + " canceled");
                    focusedDownload.setDownloading(false);
                    System.out.println("canceled");
                    break;
                case "settingBtn":
                case "Settings":
                    System.out.println("settings");
                    settingsFrame();
                    break;
                case "folderBtn":
                    System.out.println(focusedDownload.getFileName() + " folder opened");
                    Desktop desktop;
                    if (!Desktop.isDesktopSupported()) {
                        System.out.println("desktop is not supported");
                        return;
                    }
                    desktop = Desktop.getDesktop();
                    String path = focusedDownload.getSaveLocation();
                    //System.out.println(System.getProperty("user.dir"));
                    try {
                        File fPath = new File(path);
                        if (!fPath.exists()) {
                            System.out.println("path nist");
                        }
                        if (!fPath.isDirectory()) {
                            System.out.println("dir nist");
                        }
                        desktop.open(fPath);
                    } catch (IOException e) {
                        System.out.println(e);
                        e.printStackTrace();
                    }
                    break;
                case "By Name":
                case "By Size":
                case "By Start Time":
                    if (!sortBy.contains(mouseEvent.getComponent().getName())) {
                        sortBy.add(mouseEvent.getComponent().getName());
                        ((JCheckBoxMenuItem) mouseEvent.getComponent()).setState(true);
                    } else {
                        sortBy.remove(mouseEvent.getComponent().getName());
                        ((JCheckBoxMenuItem) mouseEvent.getComponent()).setState(false);
                    }
                    mouseEvent.getComponent().getParent().setVisible(true);
                    System.out.println(sortBy);
                    doSort();
                    break;
                case "Sort Descending":
                    if (!isDescending) {
                        isDescending = true;
                        ((JCheckBoxMenuItem) mouseEvent.getComponent()).setState(true);
                    } else {
                        isDescending = false;
                        ((JCheckBoxMenuItem) mouseEvent.getComponent()).setState(false);
                    }
                    mouseEvent.getComponent().getParent().setVisible(true);
//                    System.out.println(isDescending);
//                    System.out.println(((JCheckBoxMenuItem)mouseEvent.getComponent()).getState());
                    doSort();
                    break;
                case "Export":
                    System.out.println("export");
                    FileManager.export();
                    break;
                case "Processing":
                case "Completed":
                case "Queues":
                case "Restore":
                    for (int i = 0; i < showInfo.size(); i++)
                        if (showInfo.get(i)) {
                            rightPanel.remove(downloadInfoPanel);
                            showInfo.set(i, false);
                        }
                    leftBarSelected = mouseEvent.getComponent().getName();
                    downloadPanel.remove(downloadInnerPanel);
                    downloadPanel.repaint();
                    downloadInnerPanel = rightPanelMaker(leftBarSelected);
                    downloadPanel.add(downloadInnerPanel, BorderLayout.NORTH);
                    downloadPanel.repaint();
                    frame.revalidate();
                    System.out.println(leftBarSelected + " tab");
                    break;
                case "addToQueue":
                    queue.add(focusedDownload);
                    queueShowInfo.add(false);
                    break;
                case "removeFromQueue":
                    queue.remove(focusedDownload);
                    queueShowInfo.remove(queue.indexOf(focusedDownload));
                    downloadPanel.remove(downloadInnerPanel);
                    downloadPanel.repaint();
                    downloadInnerPanel = rightPanelMaker(leftBarSelected);
                    downloadPanel.add(downloadInnerPanel, BorderLayout.NORTH);
                    downloadPanel.repaint();
                    frame.revalidate();
                    break;
                case "startQueue":
                    if (!DownloadManager.isQueueStarted())
                        DownloadManager.startQueue(queue);
                    else
                        DownloadManager.stopQueue(queue);
//                    mouseEvent.getComponent().
                    break;
                case "Move Up":
                    if (queue.indexOf(focusedDownload) != 0) {
                        Collections.swap(queue, queue.indexOf(focusedDownload), queue.indexOf(focusedDownload) - 1);
                        Collections.swap(queueShowInfo, queueShowInfo.indexOf(focusedDownload), queueShowInfo.indexOf(focusedDownload) - 1);
                    }
                    downloadPanel.remove(downloadInnerPanel);
                    downloadPanel.repaint();
                    downloadInnerPanel = rightPanelMaker(leftBarSelected);
                    downloadPanel.add(downloadInnerPanel, BorderLayout.NORTH);
                    downloadPanel.repaint();
                    frame.revalidate();
                    break;
                case "Move Down":
                    if (queue.indexOf(focusedDownload) != queue.size() - 1) {
                        Collections.swap(queue, queue.indexOf(focusedDownload), queue.indexOf(focusedDownload) + 1);
                        Collections.swap(queueShowInfo, queueShowInfo.indexOf(focusedDownload), queueShowInfo.indexOf(focusedDownload) + 1);
                    }
                    downloadPanel.remove(downloadInnerPanel);
                    downloadPanel.repaint();
                    downloadInnerPanel = rightPanelMaker(leftBarSelected);
                    downloadPanel.add(downloadInnerPanel, BorderLayout.NORTH);
                    downloadPanel.repaint();
                    frame.revalidate();
                    break;
                default:
                    break;
            }
        }

        @Override
        public void mousePressed(MouseEvent mouseEvent) {
            if (mouseEvent.getComponent() instanceof JButton)
                mouseEvent.getComponent().setBackground(mouseEvent.getComponent().getBackground().brighter());
        }

        @Override
        public void mouseEntered(MouseEvent mouseEvent) {
            if (mouseEvent.getComponent() instanceof JButton) {
                ((JButton) mouseEvent.getComponent()).setContentAreaFilled(true);
                mouseEvent.getComponent().setBackground(mouseEvent.getComponent().getBackground().brighter());
            }
        }

        @Override
        public void mouseExited(MouseEvent mouseEvent) {
            if (mouseEvent.getComponent() instanceof JButton) {
                ((JButton) mouseEvent.getComponent()).setContentAreaFilled(false);
                mouseEvent.getComponent().setBackground(mouseEvent.getComponent().getParent().getBackground());
            }
        }

        @Override
        public void mouseClicked(MouseEvent mouseEvent) {
        }
    }
    private class RadioButtonActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent event) {
            JRadioButton button = (JRadioButton) event.getSource();
            for (UIManager.LookAndFeelInfo each : UIManager.getInstalledLookAndFeels()) {
                if (button.getText().equals(each.getName()))
                    setLookAndFeel(each.getClassName());
            }
        }
    }

    /**
     * refreshes the ui
     */
    public void refresh() {
        DownloadManager.setDownloads(downloads);
        DownloadManager.setQueue(queue);
        this.downloadPanel.remove(this.downloadInnerPanel);
        this.downloadPanel.repaint();
        this.downloadInnerPanel = rightPanelMaker(this.leftBarSelected);
        this.downloadPanel.add(this.downloadInnerPanel, BorderLayout.NORTH);
        this.frame.revalidate();
    }

    /**
     * returns this!
     * @return
     */
    private GUI returnGUI() {
        return this;
    }

    /**
     * returns the limit
     * @return
     */
    public static int getLimitForSimultaneouslyDownload() {
        return limitForSimultaneouslyDownload;
    }

    /**
     * rounds float numbers
     * @param num
     * @return rounded num
     */
    private float rounder(float num){
        return ((int)(num*1000))/(float)1000;
    }
}




