import java.io.Serializable;
import java.util.ArrayList;

public class DownloadList implements Serializable {
    private ArrayList<Download> downloads;

    public DownloadList() {
        downloads = new ArrayList<>();
    }

    public void addDownload(Download download){
        this.downloads.add(download);
    }

    public ArrayList<Download> getDownloads() {
        return downloads;
    }
}
